document.addEventListener("DOMContentLoaded", function(event) { 
	
  // Slider homescreen
  var slider = Peppermint(document.getElementById('peppermint'), {
    slideshow: true,
    speed: 500,
    dots: true,
    slideshowInterval: 5000,
    stopSlideshowAfterInteraction: true
  });

// Slideout menu
var slideout = new Slideout({
  'panel': document.getElementById('panel'),
  'menu': document.getElementById('menu'),
  'padding': 256,
  'tolerance': 70
});

// Toggle button
document.querySelector('.toggle-button').addEventListener('click', function() {
  slideout.toggle();
});

function close(eve) {
  eve.preventDefault();
  slideout.close();
}

slideout
.on('beforeopen', function() {
  this.panel.classList.add('panel-open');
})
.on('open', function() {
  this.panel.addEventListener('click', close);
})
.on('beforeclose', function() {
  this.panel.classList.remove('panel-open');
  this.panel.removeEventListener('click', close);
});
});


// Counter
var tableValue = document.querySelector('.cart-table-counter__value');
var btnMinus = document.querySelector('.js-cart-minus');
var btnPlus = document.querySelector('.js-cart-plus');

btnMinus.addEventListener('click', function(){
  tableValue.textContent--;
  if(tableValue.textContent <= -1) {
    tableValue.textContent = 0;
  }
});

btnPlus.addEventListener('click', function(){
  tableValue.textContent++;
});
